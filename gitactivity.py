import operator

 
class gitactivity():
    def __init__(self, type):
        self.ipaddresses = {}
        self.repositories = {}
        self.users = {}
        self.type = type
        self.counter = 0

    def display_result(self, result, howMany, title):
        print("==========================================")
        result = result[:howMany]
        print("Top {} {}".format(howMany, title))
        for key, value in result:
            print("{1:12,d} - {0:}".format(key, value))
        print(" ")

    def jira_result(self, result, howMany, title):
        print("h2. ==========================================")
        result = result[:howMany]
        print("h3. Top {} {}".format(howMany, title))
        print("{noformat}")
        for key, value in result:
            print("{1:12,d} - {0:}".format(key, value))
        print("{noformat}")
        print(" ")

    def file_result(self, result, howMany, title, outputFile):
        pass

    def display(self, howMany, output, outputFile):
        print("h1. {}".format(self.type))
        print("h2. Total Requests: {0:8,d} ".format(self.counter))

        ip_result = sorted(self.ipaddresses.items(), key=operator.itemgetter(1), reverse=True)
        repo_result = sorted(self.repositories.items(), key=operator.itemgetter(1), reverse=True)
        user_result = sorted(self.users.items(), key=operator.itemgetter(1), reverse=True)

        if output == "screen":
            self.display_result(ip_result, howMany, "IP Addresses")
            self.display_result(repo_result, howMany, "Repositories")
            self.display_result(user_result, howMany, "Users")
        elif output == "jira":
            self.jira_result(ip_result, howMany, "IP Addresses")
            self.jira_result(repo_result, howMany, "Repositories")
            self.jira_result(user_result, howMany, "Users")
        else:
            self.file_result(ip_result, howMany, "IP Addresses", outputFile)
            self.file_result(repo_result, howMany, "Repositories", outputFile)
            self.file_result(user_result, howMany, "Users", outputFile)

    def add_entry(self, ip, repo, user):
        ipaddress = self.ipaddresses.setdefault(ip, 0)
        self.ipaddresses[ip] = ipaddress + 1
        repository = self.repositories.setdefault(repo, 0)
        self.repositories[repo] = repository + 1
        username = self.users.setdefault(user, 0)
        self.users[user] = username + 1
        self.counter += 1
